<?php

declare(strict_types=1);

namespace app\domain;

use app\domain\port\SaveNotePort;

final class ShareNotesUseCase
{
    private ShareNotesInput $input;
    private ShareNotesOutput $output;

    private function __construct(private readonly SaveNotePort $notes_store)
    {
    }

    public static function create(SaveNotePort $notes_store): self
    {
        return new self($notes_store);
    }

    public function input(ShareNotesInput $shareNotesInput): self
    {
        $this->input = $shareNotesInput;

        return $this;
    }

    public function execute(): void
    {
        $ids = [];
        foreach ($this->input->notes() as $note) {
            $ids[] = $this->notes_store->save_note($note);
        }

        $this->output = ShareNotesOutput::create($ids);
    }

    public function output(): ShareNotesOutput
    {
        return $this->output;
    }
}
