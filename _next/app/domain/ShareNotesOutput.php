<?php

declare(strict_types=1);

namespace app\domain;

use app\domain\port\OutputPort;

final class ShareNotesOutput implements OutputPort
{
    private function __construct(private readonly array $data)
    {
    }

    public static function create(array $data): self
    {
        return new self($data);
    }

    public function data(): array
    {
        return $this->data;
    }
}
