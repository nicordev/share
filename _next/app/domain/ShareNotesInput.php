<?php

declare(strict_types=1);

namespace app\domain;

use app\domain\port\InputPort;

final class ShareNotesInput implements InputPort
{
    private array $notes;

    private function __construct(private readonly array $data)
    {
        $this->notes = $data['notes'] ?? [];
    }

    public static function create(array $data): self
    {
        return new self($data);
    }

    public function notes(): array
    {
        return $this->notes;
    }
}
