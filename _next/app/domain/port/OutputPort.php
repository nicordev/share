<?php

declare(strict_types=1);

namespace app\domain\port;

interface OutputPort
{
    public static function create(array $data): self;

    public function data(): array;
}
