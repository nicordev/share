<?php

declare(strict_types=1);

namespace app\domain\port;

interface SaveNotePort
{
    public function save_note(array $note): string;
}