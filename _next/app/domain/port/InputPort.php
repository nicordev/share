<?php

declare(strict_types=1);

namespace app\domain\port;

interface InputPort
{
    public static function create(array $data): self;
}
