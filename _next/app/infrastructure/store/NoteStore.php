<?php

declare(strict_types=1);

namespace app\infrastructure\store;

use PDO;
use app\domain\port\SaveNotePort;

final class NoteStore implements SaveNotePort
{
    public function __construct(
        private readonly PDO $database_manager,
    ) {
    }

    public function save_note(array $note): string
    {
        $sql = <<< SQL
            insert into note (title, content)
            values (:title, :content)
        SQL;
        $statement = $this->database_manager->prepare($sql);
        $statement->bindParam('title', $note['title']);
        $statement->bindParam('content', $note['content']);
        $statement->execute();

        return (string) $this->database_manager->lastInsertId();
    }
}
