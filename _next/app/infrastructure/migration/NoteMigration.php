<?php

declare(strict_types=1);

namespace app\infrastructure\migration;

use PDO;

final class NoteMigration
{
    public function __construct(private readonly PDO $database_manager)
    {
    }

    public function up(): void
    {
        $sql = <<< SQL
            create table if not exists note (
                id integer primary key
                , title text not null
                , content text
            )
        SQL;
        $this->database_manager->exec($sql);
    }

    public function down(): void
    {
        $sql = <<< SQL
            drop table if exists note
        SQL;
        $this->database_manager->exec($sql);
    }
}