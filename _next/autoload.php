<?php

spl_autoload_register(function (string $fqcn) {
    $namespace_paths = [
        'app\\domain\\' => __DIR__.'/app/domain',
        'app\\domain\\port\\' => __DIR__.'/app/domain/port',
        'app\\infrastructure\\store\\' => __DIR__.'/app/infrastructure/store',
        'app\\infrastructure\\migration\\' => __DIR__.'/app/infrastructure/migration',
        'app\\tests\\' => __DIR__.'/tests',
    ];

    foreach ($namespace_paths as $namespace => $path) {
        $class_name = str_replace($namespace, '', $fqcn);
        $file = sprintf(
            '%s/%s.php',
            $path,
            $class_name,
        );

        if (is_file($file)) {
            require_once($file);

            return true;
        }

        $test_file = sprintf(
            '%s/%s.test.php',
            $path,
            $class_name,
        );

        if (is_file($test_file)) {
            require_once($test_file);

            return true;
        }
    }

	return false;
});
