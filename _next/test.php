<?php

declare(strict_types=1);

namespace test;

use Throwable;
use RuntimeException;
use DirectoryIterator;
use InvalidArgumentException;
use LogicException;

// autoloader
require __DIR__.'/autoload.php';

/**
 * The only function needed in test files
 *
 * Add this line on top of the test file to use it:
 *  use function test\test;
 */
function test(string $description, callable $test, array $cases) {
    $test_runner = test_runner::instance();

    $test_runner->add_test([
        'description' => $description,
        'test' => $test,
        'cases' => $cases,
    ]);
}

/**
 * To throw some exceptions in tests
 */
class test_exception  extends RuntimeException
{}

/**
 * To do some assertions in tests
 */
final class it
{
    public static function is_not_same($expected, $actual, ?string $message = null): void
    {
        $message = $message ?? "Assertion failed: NOT SAME\nActual:\n".var_export($actual, true)."\nExpected:\n".var_export($expected, true);

        if ($actual === $expected) {
            throw new test_exception($message);
        }
    }

    public static function is_same($expected, $actual, ?string $message = null): void
    {
        $message = $message ?? "Assertion failed: SAME\nActual:\n".var_export($actual, true)."\nExpected:\n".var_export($expected, true);

        if ($actual !== $expected) {
            throw new test_exception($message);
        }
    }

    public static function is_equal($expected, $actual, ?string $message = null): void
    {
        $message = $message ?? "Assertion failed: SAME\nActual:\n".var_export($actual, true)."\nExpected:\n".var_export($expected, true);

        if ($actual != $expected) {
            throw new test_exception($message);
        }
    }

    public static function is_true(bool $actual, ?string $message = null): void
    {
        $message = $message ?? "Assertion failed: false is not true.\n";

        if ($actual !== true) {
            throw new test_exception($message);
        }
    }

    public static function is_false($actual, $message = 'Assertion failed: False is not True.'): void
    {
        if ($actual !== false) {
            throw new test_exception($message);
        }
    }

    public static function is_like($expectedPattern, $actual, $message = 'Assertion failed.'): void
    {
        if (!preg_match($expectedPattern, $actual)) {
            throw new test_exception($message);
        }
    }
}

final class test_runner
{
    /**
     * @var array{description: string, test: callable, cases: array<array<mixed>>}
     */
    private array $tests;
    private array $fails = [];
    private array $statistics;
    /**
     * @var array<string>
     */
    private array $filters = [];
    private static self $instance;

    private function __construct()
    {
    }

    public static function instance(): self
    {
        if (empty(test_runner::$instance)) {
            test_runner::$instance = new self();
        }

        return test_runner::$instance;
    }

    /**
     * @param array{description: string, test: callable, cases: array<array<mixed>>} $test
     */
    public function add_test(array $test): void
    {
        $this->tests[] = $test;
    }

    public function add_filter(string $filter): void
    {
        $this->filters[] = $filter;
    }

    private function can_be_run(array $test): bool
    {
        if ($this->filters === []) {
            return true;
        }

        foreach ($this->filters as $filter) {
            if (false !== strpos($test['description'], $filter)) {
                return true;
            }
        }

        return false;
    }

    public function run(): void
    {
        if (empty($this->tests)) {
            throw new LogicException('No test to run.');
        }

        $tests_to_run = array_filter($this->tests, [$this, 'can_be_run']);

        $this->statistics = [
            'success_count' => 0,
            'fails_count' => 0,
            'cases_count' => count($tests_to_run),
        ];

        foreach ($tests_to_run as $test) {
            try {
                if ([] === $test['cases']) {
                    $test['test']();
                } else {
                    foreach ($test['cases'] as $case) {
                        if (!is_array($case)) {
                            throw new InvalidArgumentException(sprintf('The test case must be an array, %s given.', gettype($case)));
                        }
                        $test['test'](...$case);
                    }
                }
            } catch (Throwable $exception) {
                $this->print_error('x');
                $this->fails[] = [
                    'description' => $test['description'],
                    'reason' => $exception->getMessage(),
                    'cases' => $test['cases'] ?? [],
                    'error' => $exception,
                ];
                ++$this->statistics['fails_count'];

                continue;
            }

            $this->print_success('.');
            ++$this->statistics['success_count'];
        }

        $this->print_info("\n{$this->statistics['success_count']} / {$this->statistics['cases_count']}\n");

        if ($this->statistics['success_count'] === $this->statistics['cases_count']) {
            $this->print_success("\n\n  All tests successful! Have a nice day :)  \n\n");

            return;
        }

        foreach ($this->fails as $fail) {
            $this->print_warning("\n  {$fail['description']}  \n", false);
            $this->print_error("\n  {$fail['reason']}  \n\n");
            if (!empty($fail['cases'])) {
                \var_export($fail['cases']);
            }
        }

        throw $this->fails[0]['error'];
    }

    private function print_info(string $message, bool $foreground = true): void
    {
        $ground = $foreground ? 3 : 4;
        echo "\033[{$ground}4m$message\033[0m";
    }

    private function print_success(string $message, bool $foreground = true): void
    {
        $ground = $foreground ? 3 : 4;
        echo "\033[{$ground}2m$message\033[0m";
    }

    private function print_warning(string $message, bool $foreground = true): void
    {
        $ground = $foreground ? 3 : 4;
        echo "\033[{$ground}3m$message\033[0m";
    }

    private function print_error(string $message, bool $foreground = true): void
    {
        $ground = $foreground ? 3 : 4;
        echo "\033[{$ground}1m$message\033[0m";
    }
}

function run_tests(array $test_directories_or_files): void
{
    foreach ($test_directories_or_files as $test_directory_or_file) {
        echo "\033[44;37m  Running tests from $test_directory_or_file  \033[0m\n";

        if (is_file($test_directory_or_file)) {
            require_once $test_directory_or_file;
            continue;
        }

        foreach (new DirectoryIterator($test_directory_or_file) as $file) {
            if ($file->isDot()) continue;
            if ($file->isDir()) continue;

            require_once $file->getRealPath();
        }
    }

    test_runner::instance()->run();
}

define('SCRIPT_NAME', pathinfo($argv[0], PATHINFO_BASENAME));

if ($argc < 2) {
    echo sprintf(
        <<< MESSAGE

        %s \033[33m test_directory_or_file [test_directory_or_file]\033[0m

        -f|--filter some_string_here

        MESSAGE,
        SCRIPT_NAME,
    );
    exit();
}

// remove script name from arguments
\array_shift($argv);

// add filters
foreach ($argv as $key => $value) {
    if ($value === '-f' || $value === '--filter') {
        test_runner::instance()->add_filter($argv[$key + 1]);
        unset($argv[$key]);
        unset($argv[$key + 1]);
    }
}

run_tests($argv);
