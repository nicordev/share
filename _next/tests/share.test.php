<?php

declare(strict_types=1);

namespace tests;

use function test\test;

use app\domain\port\OutputPort;
use test\it;
use app\domain\ShareNotesInput;
use app\domain\ShareNotesUseCase;
use app\infrastructure\migration\NoteMigration;
use app\infrastructure\store\NoteStore;
use PDO;

final class TestDatabaseHandler
{
    private readonly PDO $database_manager;
    private readonly NoteMigration $note_migration;

    public function __construct(private readonly string $database)
    {
    }

    public function setup_sqlite(): void
    {
        $this->database_manager = new PDO(
            dsn: 'sqlite:'.$this->database,
            options: [
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
            ]
        );
        $this->note_migration = new NoteMigration($this->database_manager());
        $this->migrate();
    }

    public function migrate(): void
    {
        $this->note_migration->down($this->database);
        $this->note_migration->up($this->database);
    }

    public function database_manager(): PDO
    {
        return $this->database_manager;
    }

    public function database(): string
    {
        return $this->database;
    }
}

$test_database_handler = new TestDatabaseHandler('my_share_test');
$test_database_handler->setup_sqlite();

test(
    description: 'can share notes',
    test: function (array $input_data) use ($test_database_handler) {
        $input = ShareNotesInput::create($input_data);
        $shareUseCase = ShareNotesUseCase::create(
            new NoteStore(
                $test_database_handler->database_manager(),
                $test_database_handler->database(),
            ),
        );
        $shareUseCase->input($input);
        $shareUseCase->execute();
        $output = $shareUseCase->output();

        it::is_true(!empty($output));
        it::is_true($output instanceof OutputPort, 'wrong class.');
        it::is_same(
            count($input_data['notes']),
            count($output->data()),
            'wrong output count.'
        );
    },
    cases: [
        [
            [
                'notes' => [
                    [
                        'title' => 'my note 1',
                        'content' => 'my note 1 content',
                    ],
                ]
            ]
        ]
    ],
);
